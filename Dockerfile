FROM java:8
ADD target/linkedinloginservice.jar linkedinloginservice.jar
ENTRYPOINT ["java","-jar","linkedinloginservice.jar"]
