package com.wavelabs.login.services;

import com.wavelabs.login.user.User;

/**
 * 
 * @author thejasreem
 * UserService is for service call.
 * createNewUser is the method for creating new User.
 * @Object User
 */
public interface UserService {
public boolean createNewUser(User user);
public User getProfile(Integer userId);
}
