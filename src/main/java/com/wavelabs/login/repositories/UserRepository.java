package com.wavelabs.login.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.wavelabs.login.user.User;

/**
 * 
 * @author thejasreem
 * UserRepository is used for persistence.
 */
@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

	User findByEmail(String email);

	User findById(Integer userId);



	
	

}
