package com.wavelabs.login.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wavelabs.login.repositories.UserRepository;
import com.wavelabs.login.services.UserService;
import com.wavelabs.login.user.User;

@Service
public class UserServiceImpl implements UserService {
	static Logger log = Logger.getLogger(UserServiceImpl.class);
	@Autowired
	UserRepository userRepo;

	@Override
	public boolean createNewUser(User user) {
		try {
			userRepo.save(user);
			return true;
		} catch (Exception e) {
			log.error(e);
			return false;
		}
	}

	@Override
	public User getProfile(Integer userId) {
		try {
			
			User user = userRepo.findById(userId);
			return user;
		} catch (Exception e) {
			log.error(e);
			return null;
		}
	}
}
