package com.wavelabs.login.controller;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.wavelabs.login.repositories.UserRepository;
import com.wavelabs.login.services.UserService;
import com.wavelabs.login.user.User;

import io.nbos.capi.api.v0.models.RestMessage;

@RestController
@Component
@RequestMapping(value = "/login")
public class LoginController {
	static final Logger log = Logger.getLogger(LoginController.class);

	@Value("${redirectUrl}")
	private String redirectUrl;
	@Value("${clientId}")
	private String clientId;
	@Value("${clientSecret}")
	private String clientSecret;
	@Value("${accessTokenUrl}")
	private String accessTokenUrl;
	@Value("${profileUrl}")
	private String profileUrl;

	@Autowired
	UserService userService;
	@Autowired(required = true)
	private UserRepository userRepo;

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/{authcode}")
	public ResponseEntity getAuthCode(@PathVariable("authcode") String authcode) throws JsonProcessingException {
		// method for getting headers
		HttpEntity entity = setHeaders(authcode);
		// method for getting access token (jsonObject)
		String jaccessToken = getAccessToken(entity);
		// parsing the json object to get access token
		String accessToken = parseJsonToGetAccessToken(jaccessToken);
		// method for getting email
		Integer userId = getProfile(accessToken);
		RestMessage restMessage = new RestMessage();
		ResponseEntity<Integer> response = new ResponseEntity<>(userId, HttpStatus.OK);
		//System.out.println(response.getBody());
		//System.out.println("response-"+response);
		if (userId != 0) {
			
			return ResponseEntity.status(200).body(response);
		
		} else {
			restMessage.message = "Retrieval of Profile Fails!";
			restMessage.messageCode = "404";
			return ResponseEntity.status(404).body(restMessage);
		}

	}

	@SuppressWarnings("rawtypes")
	@RequestMapping(value = "/profile/{userId}", method = RequestMethod.GET)
	public ResponseEntity getProfileByEmail(@PathVariable Integer userId) {
		User user = userService.getProfile(userId);
		//System.out.println("user id -"+user.getId());
		RestMessage restMessage = new RestMessage();

		if (user != null) {
			return ResponseEntity.status(200).body(user);
		} else {
			restMessage.message = "Name not found!";
			restMessage.messageCode = "404";
			return ResponseEntity.status(404).body(restMessage);
		}
	}

	// redirect url
	@SuppressWarnings("rawtypes")
	@RequestMapping(method = RequestMethod.GET, value = "/status")
	public ResponseEntity getLoginStatus() {
		return ResponseEntity.status(HttpStatus.ACCEPTED).build();

	}

	private Integer getProfile(String accessToken) throws JsonProcessingException {
		RestTemplate restTemplate = new RestTemplate();
		// set headers
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.set("Connection", "Keep-Alive");
		requestHeaders.set("Authorization", "Bearer " + accessToken);
		requestHeaders.set("Host", "api.linkedin.com");
		HttpEntity<String> entity = new HttpEntity<>(requestHeaders);
		@SuppressWarnings("rawtypes")
		ResponseEntity response = restTemplate.exchange(profileUrl, HttpMethod.GET, entity, String.class);
		String jsonString = (String) response.getBody();
		// parse the response object to get user profile
		JSONParser jParser = new JSONParser();
		String email = "";
		Integer userId = 0;
		try {
			JSONObject jObject = (JSONObject) (jParser.parse(jsonString));
			email = (String) jObject.get("emailAddress");
			String firstName = (String) jObject.get("firstName");
			String lastName = (String) jObject.get("lastName");
			String image = (jObject.get("pictureUrl") == null) ? null : (String) jObject.get("pictureUrl");
			User user = userRepo.findByEmail(email);
			// if email doesn't exists in db save the user object in db
			if (user == null) {
				user = new User();
				user.setEmail(email);
				user.setFirstname(firstName);
				user.setLastname(lastName);
				user.setImage(image);
				userRepo.save(user);
			}
			 userId = user.getId();
			// System.out.println("userid-" + userId);
		} catch (ParseException e) {
			log.error(e);
		}
		return userId;

	}

	RestTemplate restTemplate = new RestTemplate();

	@SuppressWarnings("rawtypes")
	public HttpEntity setHeaders(String authcode) {
		// Create the request body as a MultiValueMap set all parameters
		MultiValueMap<String, String> headersMap = new LinkedMultiValueMap<>();
		headersMap.add("grant_type", "authorization_code");
		headersMap.add("code", authcode);
		headersMap.add("redirect_uri", redirectUrl);
		headersMap.add("client_id", clientId);
		headersMap.add("client_secret", clientSecret);
		RestTemplate restTemplate = new RestTemplate();
		// set the header
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		HttpEntity entity = new HttpEntity<MultiValueMap<String, String>>(headersMap, requestHeaders);
		restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
		return entity;
	}

	@SuppressWarnings("rawtypes")
	public String getAccessToken(HttpEntity entity) {
		String jaccessToken = "";
		try {
			ResponseEntity<String> response = restTemplate.exchange(accessTokenUrl, HttpMethod.POST, entity,
					String.class);
			jaccessToken = response.getBody();
		} catch (HttpClientErrorException e) {
			log.error(e);
			
			  System.out.println("Raw status code - " + e.getRawStatusCode());
			  System.out.println("Status code -" + e.getStatusCode());
			  System.out.println("Status text - " + e.getStatusText());
			  System.out.println("Response Headers - " +
			  e.getResponseHeaders()); System.out.println("Message - " +
			  e.getMessage()); System.out.println("Localized message - " +
			  e.getLocalizedMessage()); System.out.println("Cause - " +
			  e.getCause()); System.out.println("Most specific cause - " +
			  e.getMostSpecificCause()); System.out.println("Root cause - " +
			  e.getRootCause()); System.out.println("Suppressed - " +
			  e.getSuppressed());
			  System.out.println("Response Body as String - " +
			  e.getResponseBodyAsString());
			  System.out.println("Res body as byte array -" +
			  e.getResponseBodyAsByteArray());
			 
		}
		return jaccessToken;
	}

	public String parseJsonToGetAccessToken(String accessToken) {
		// parse the response to get access token
		String jObjAccessToken = "";
		JSONParser jParser = new JSONParser();

		try {
			JSONObject jObject = (JSONObject) (jParser.parse(accessToken));
			jObjAccessToken = (String) jObject.get("access_token");
		} catch (ParseException e) {
			log.error(e);
		}
		return jObjAccessToken;
	}

}
